# benchmarks

Automatic benchmarks for Bamboo.

Within the `tests` directory is each test. The format is `tests/<test name>/<test name>.<file extension>`.

Currently, Bamboo, Python, and C++ are tested against each other.

## Results

See the job pipeline to see the results. Each has a `results.txt` artifact, which contains the output of the test.
