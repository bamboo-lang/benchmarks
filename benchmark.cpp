#include <cstdlib>
#include <string>
#include <algorithm>
#include <numeric>
#include <iostream>
#include <chrono>
#include <cstring>
#include <vector>
#include <unordered_map>

const auto avg = [] (std::vector<double> v) { return std::accumulate(v.begin(), v.end(), 0.0) / v.size(); };

std::vector<double> performTest(const char* cmd, int tests = 50, const bool verbose = false)
{
	std::cout << "Testing " << cmd << std::endl;
	std::chrono::duration<double> fastest;
	std::vector<double> times;

	for (int i = 0; i < tests; i++)
	{
		// Set some time points
		std::chrono::time_point<std::chrono::system_clock> start, end;

		// Run and time everything
		start = std::chrono::system_clock::now();
		int r = system(cmd);
		end = std::chrono::system_clock::now();
		if (r)
		{
			std::cout << "An error occured in this test. Cancelling further tests." << std::endl;
			return {-1};
		}

		// The total time of execution
		std::chrono::duration<double> time = end - start;
		auto x = time.count();
		times.push_back(time.count());;
		// Print the execution time.
		if (verbose) std::cout << "-> test #" << i+1 << " took " << time.count() << std::endl;
	}

	std::sort(times.begin(), times.end());
	return times;
}

std::vector<double> test(std::string folderName, int tests = 50, const bool verbose = false)
{
	// Compile the C++ tests
	std::cout << "Compiling C++ tests." << std::endl;
	system(("g++ tests/" + folderName + "/" + folderName + ".cpp -o ./tests/" + folderName + "/" + folderName + ".out").c_str());
	std::cout << "Done." << std::endl;

	// By doing the string->const char* conversion here, I can mitigate any time added from .c_str()
	std::string bamPath	= "~/.bamboo/bin/bamboo run tests/" + folderName + "/" + folderName + ".bam -q";
	std::string cppPath	= "./tests/" + folderName + "/" + folderName + ".out";
	std::string pyPath	= "python3 tests/" + folderName + "/" + folderName + ".py";

	// Start testing!
	auto bamDurations = performTest(bamPath.c_str(), tests, verbose);
	auto cppDurations = performTest(cppPath.c_str(), tests, verbose);
	auto pyDurations  = performTest(pyPath.c_str() , tests, verbose);

	auto bamFastest = bamDurations[0];
	auto cppFastest = cppDurations[0];
	auto pyFastest 	= pyDurations[0];

	return {bamFastest, avg(bamDurations), cppFastest, avg(cppDurations), pyFastest, avg(pyDurations)};
}

int main(const int argc, const char** argv)
{
	int tests = std::stoi(argv[1]);
	std::unordered_map<std::string, std::vector<double>> results;

	std::cout << "\033[32m---[ TESTING FIB ]---\033[0m" << std::endl;
	results.insert({"fib", test("fib", tests, true)});

	std::cout << "\033[32m---[ TESTING HELLOWORLD ]---\033[0m" << std::endl;
	results.insert({"helloworld", test("helloworld", tests, true)});

	std::cout << "\033[33m--- VERSIONS ---\033[0m" << std::endl;
	std::cout << "\033[32m- Bamboo -\033[0m" << std::endl;
	system("cd bamboo && git branch -v");
	std::cout << "\033[32m- Python -\033[0m" << std::endl;
	system("python3 --version");
	std::cout << "\033[32m- G++ -\033[0m" << std::endl;
	system("g++ --version | grep 'g++'");

	std::cout << "\033[33m--- RESULTS ---\033[0m" << std::endl;
	for (std::pair<std::string, std::vector<double>> pair : results)
	{
		auto itr = pair.second.begin();
		double
			bamFastest = *itr,
			bamAvg = *(++itr),
			cppFastest = *(++itr),
			cppAvg = *(++itr),
			pyFastest = *(++itr),
			pyAvg = *(++itr);

		// Print the results of each test.
		std::cout << "\033[31m-- " << pair.first << " --\033[0m" << std::endl;
		std::cout << "\033[1;32mBamboo:\033[0m" << std::endl;
			std::cout << "\tFastest time for Bamboo:  " << bamFastest << std::endl;
			std::cout << "\tAverage time for Bamboo:  " << bamAvg << std::endl;
		std::cout << "\033[1;34mC++:\033[0m" << std::endl;
			std::cout << "\tFastest time for C++:     " << cppFastest << std::endl;
			std::cout << "\tAverage time for C++:     " << cppAvg << std::endl;
		std::cout << "\033[1;33mPython:\033[0m" << std::endl;
			std::cout << "\tFastest time for Python:  " << pyFastest << std::endl;
			std::cout << "\tAverage time for Python:  " << pyAvg << std::endl;
	}
}
