#!/usr/bin/env sh
git clone https://gitlab.com/bamboo-lang/bamboo && cd bamboo && python3 install.py && cd ..
g++ benchmark.cpp -o a.out
./a.out 100 | tee results.txt